//=================================================================================================================
// Fonction générale
//=================================================================================================================

// Nombre de valeurs aléatoires
var TAILLE_ALE = 100000;


/**
 * Retourne un tableau de va
 */
 function tabRnadom (nbAL){
 	var tableauVar =  new Array(nbAL);

 	for(var i=0;i<nbAL;i++){
 		tableauVar[i]=Math.random();
 	}

 	return tableauVar;
 }



/**
 * Indique le nombre de chiffre ce situant dans l'intervalle (loi uniforme)
 */
 function nbIn(minI,maxI,valTab){
 	var compteur=0;
 	for(var i=0;i<valTab.length;i++){
 		if(valTab[i]>minI && valTab[i]<maxI){
 			compteur++;
 		}
 	}
 	return compteur;
 }


//=================================================================================================================
//loi uniforme

/**
 * Génere nbAL aléatoire et les places dans un tableau
 */
 function vaUniforme(nbAL){

	// Création du tableau de valeur aléatoires
	var tableauVar =  new Array(nbAL);

	for(var i=0;i<nbAL;i++){
		tableauVar[i]=Math.random();
	}

	return tableauVar;
}


var tabAL = vaUniforme(TAILLE_ALE);


//=================================================================================================================
//loi expo

/**
 * Genere un nombre nb  va  suivant la loi exp p74
 */
 function vaExp(nbAL,Y){
	// Création du tableau de valeur aléatoires
	var tableauVar =  new Array(nbAL);

	for(var i=0;i<nbAL;i++){
		tableauVar[i]=(-1/Y)*Math.log(Math.random());
	}

	return tableauVar;
}

var tabAlExp = vaExp(TAILLE_ALE,0.5); //Y doit eter suppérieur a 0

//=================================================================================================================
//loi de Weibull

/**
 * Genere un nombre nb  va  suivant la loi de weibull p74
 */
 function vaWeibull(nbAL,A,B){
	// Création du tableau de valeur aléatoires
	var tableauVar =  new Array(nbAL);

	for(var i=0;i<nbAL;i++){
		tableauVar[i]=Math.pow((-1*(1-((Math.random())))/Math.pow(A,B)),1/B);
	}

	return tableauVar;
}

var tabAlWeibull = vaWeibull(TAILLE_ALE,2,4);//a et b >0 (wikipédia)

//=================================================================================================================
//loi Normale

/**
 * Genere un nombre nb  va suivant la loi normale
 */
 function vaNormale(nbAL,m,o){
	var U = tabRnadom(nbAL%2==0 ? nbAL/2 : (nb/2)+1); //Soit (U , V) un couple de variables aléatoires indépendantes identiquement 
						  							// distribuées suivant toutes deux la loi uniforme standard  U ([0;1])
	var V = tabRnadom(nbAL%2==0 ? nbAL/2 : (nb/2)+1);
	var tableauVar =  new Array(nbAL);
	var i=0;

	var X,Y; //indépendantes et identiquement distribué n(0.1)

	while(i<nbAL){

		X=Math.sqrt(-2*Math.log(U[(i%2==0 ?i/2:(i/2)+1)]))*Math.cos(2*Math.PI*V[(i%2==0 ?i/2:(i/2)+1)]);
		Y=Math.sqrt(-2*Math.log(U[(i%2==0 ?i/2:(i/2)+1)]))*Math.cos(2*Math.PI*V[(i%2==0 ?i/2:(i/2)+1)]);
		
		tableauVar[i] = m+o*X;	
		tableauVar[i+1] = m+o*Y;		

		i=i+2;

	}

	if(i<nbAL){
		X=Math.sqrt(-2*Math.log(U[(i%2==0 ?i/2:(i/2)+1)]))*Math.cos(2*Math.PI*V[(i%2==0 ?i/2:(i/2)+1)]);
		tableauVar[i] = m+o*X;	
	}

	return tableauVar;	
}

var tabAlNormale = vaNormale(TAILLE_ALE,0.5,0.1);

//=================================================================================================================
//affichage des courbes
$(function() {


	//histogramme approche de fonction de répartition
	var dUniforme = [];
	for (var i = 0.1; i < 1;i += 0.1) {
		dUniforme.push([i-0.1, nbIn(i-0.1,i,tabAL) / tabAL.length / 0.1]);
	}

	var dExp = [];
	for (var i = 0.1; i < 12;i += 0.1) {
		dExp.push([i-0.1, nbIn(i-0.1,i,tabAlExp)/ tabAL.length / 0.1]);
	}

	var dWeibull = [];
	for (var i = 0.1; i < 12;i += 0.1) {
		dWeibull.push([i-0.1, nbIn(i-0.1,i,tabAlWeibull)/ tabAL.length / 0.1]);
	}	

	var dNormale = [];
	for (var i = 0.1; i < 1;i += 0.1) {
		dNormale.push([i-0.1, nbIn(i-0.1,i,tabAlNormale)/ tabAL.length / 0.1]);
	}	
	//===============================================================================================================
	// densité de probabilité //todo fonction
	var dUniformeD = [];
	for (var i = 0.1; i < 1;i += 0.1) {
		dUniforme.push([i-0.1, nbIn(i-0.1,i,tabAL) / tabAL.length / 0.1]);
	}

	var dExpD = [];
	for (var i = 0.1; i < 12;i += 0.1) {
		dExpD.push([i-0.1, (0.5*Math.exp(-0.5*i))]); // ok mais todo function
	}

	var dWeibullD = [];
	for (var i = 0.1; i < 12;i += 0.1) {
		dWeibullD.push([i-0.1, (1-Math.exp(Math.pow(-1*4*i,4)))]); //a=2 b=4
	}	

	var dNormaleD = [];
	for (var i = 0.1; i < 1;i += 0.1) {
		dNormaleD.push([i-0.1, nbIn(i-0.1,i,tabAlNormale)/ tabAL.length / 0.1]);
	}

	$.plot("#placeholder", [ {
		data: dExp,
		//lines: { show: true}
		bars: { 
			show: true,
			barWidth : 0.1 
		}
	},
	{
		data: dExpD,
		lines: { show: true}

	}

	]);


});